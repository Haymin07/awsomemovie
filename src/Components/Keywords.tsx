import React from 'react'
import { useSelector } from 'react-redux';
import { selectTimeStamp } from '../Store/timeStampSlice';

interface keyword {
  title: string,
  url : string
}

interface data {
  data: keyword[],
  pos: string
}

interface KeywordsProps {
  datas: data[]
}

export default function Keywords(props: KeywordsProps) {
  const timeStamp = useSelector(selectTimeStamp);

  let i = 0
  let found = false
  while (i < props.datas.length - 1 && !found) {
    if (
      parseFloat(props.datas[i].pos) <= timeStamp
      &&
      parseFloat(props.datas[i+1].pos) >= timeStamp
    ){
      found = true
    } else {
      i ++
    }
  }
  
  return (
    <>
      {props.datas[i].data.map((keyword, index) => {
        return (
          <p
            key={index}
          >
            <a href={keyword.url}>{keyword.title}</a>
          </p>
        )
      })}
    </>
  )
}