import React, { useEffect, useState } from 'react'
import Video from './Video'
import MapContainer from './Map/MapContainer'
import { fetchData } from '../callAxios'
import Chapters from './Chapters'
import Keywords from './Keywords'
import Chat from './Chat'
import { message } from '../typeMsg'
import { add } from '../Store/messageSlice'
import { useDispatch } from 'react-redux';

const callFetch = async (
    callBackData: (data: any) => void,
    callBackDone: (bool: boolean) => void
  ) => {
  const res = await fetchData()
  callBackData(res)
  callBackDone(true)
}

export default function Main() {
  const [fetchDone, setFetchDone] = useState<boolean>(false)
  const [fetchData, setFetchData] = useState<any>()
  const dispatch = useDispatch();

  const URL = "wss://imr3-react.herokuapp.com"
  const ws = new WebSocket(URL)
  const [connected, setConnected] = useState(false)

  function connect(): void {
    ws.onopen = onOpen
    ws.onmessage = onMessage
    ws.onclose = onClose
  }

  //indicates that the connection is ready to send and receive data
  function onOpen(): void {
    setConnected(true)
    console.log("connected to chat")
  }

  //An event listener to be called when a message is received from the server
  function onMessage(event: any): void {
    const messages : message[] = JSON.parse(event.data)
    dispatch(add(messages))
  }

  //An event listener to be called when the WebSocket connection's readyState changes to CLOSED.
  function onClose(): void {
    setConnected(false)
  }

  useEffect(() => {
    callFetch(setFetchData, setFetchDone)
    connect()
    // eslint-disable-next-line
  }, [])

  if (!fetchDone) {
    return <h1>Loading</h1>
  } else {
    if (fetchData !== -1) {
      return (
        <>
        <div id='mainBox'>
          <aside>
            <div id='chapter-div'>
              <Chapters 
                chapters={fetchData.Chapters}
              />
            </div>
            <div id='map-div'>
              <MapContainer 
                waypoints={fetchData.Waypoints}
              />
            </div>
          </aside>
          <main> 
            <div id='div-video'>
              <Video 
                movie={fetchData.Film}
              />
            </div>
            <footer>
              <Keywords 
                datas={fetchData.Keywords}
              />
            </footer>
          </main>
          <aside>
            <div id='div-chat'>
              {connected ? 
                <Chat
                  webSocket={ws}
                />
              : 
                <h2>Connexion au chat en cours...</h2>
              }
            </div>
          </aside>
        </div>
        </>
      )
    } else {
      return <h1>Erreur durant le fetch</h1>
    }
  }
}
