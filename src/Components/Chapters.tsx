import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { needSeek, selectTimeStamp, set } from '../Store/timeStampSlice';

interface chapter {
  pos: string,
  title: string
}

interface ChaptersProps {
  chapters : chapter[]
}

export default function Chapters(props: ChaptersProps) {
  const timeStamp = useSelector(selectTimeStamp);
  const dispatch = useDispatch();

  let i = 0
  let found = false
  while (i < props.chapters.length - 1 && !found) {
    if (
        parseFloat(props.chapters[i].pos) <= timeStamp
        &&
        parseFloat(props.chapters[i+1].pos) > timeStamp
      ){
      found = true
    } else {
      i ++
    }
  }

  const handleOnClick = (chapter: chapter) => {
    dispatch(set(parseFloat(chapter.pos)))
    dispatch(needSeek(true))
  }

  return (
    <ul>
      {props.chapters.map((chapter, index) => {
        return (
          <li
            key={index}
            className={index === i ? "active" : "inactive"}
          >
            <p
              onClick={() => {
                handleOnClick(chapter)
              }}
            >
              {chapter.title}
            </p>
          </li>
        )
      })}
    </ul>
  )
}
