import React, { useRef } from 'react'
import ReactPlayer from 'react-player'
import { useSelector, useDispatch } from 'react-redux';
import { selectSeek, selectTimeStamp, set, needSeek } from '../Store/timeStampSlice'

interface VideoProps {
  movie: any
}

export default function Video(props: VideoProps) {
  const timeStamp = useSelector(selectTimeStamp);
  const haveToSeek = useSelector(selectSeek);
  const dispatch = useDispatch();
  const reactPlayerRef = useRef();

  if (haveToSeek) {
    // @ts-ignore
    reactPlayerRef.current.seekTo(timeStamp, "seconds")
    dispatch(needSeek(false))
  }

  const handleProgress = () => {
    // @ts-ignore
    dispatch(set(reactPlayerRef.current.getCurrentTime()))
  }

  return (
    <ReactPlayer
      // @ts-ignore
      ref={reactPlayerRef}
      // url='FilmRoute66.mp4'
      url={props.movie.file_url}
      playing={true}
      controls={true}
      loop={false}
      muted={true}
      width="100%"
      height="100%"
      // The time between onProgress callbacks, in milliseconds
      progressInterval={3000}
      onProgress={() => handleProgress()}
      onSeek={() => handleProgress()}
    />
  )
}
