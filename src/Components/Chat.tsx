import React, { useState } from 'react'
import { needSeek, selectTimeStamp, set } from '../Store/timeStampSlice'
import { useDispatch, useSelector } from 'react-redux';
import { message } from '../typeMsg';
import { selectMessages } from '../Store/messageSlice';

interface chatProps{
  webSocket : WebSocket
}

export default function Chat(props: chatProps) {
  const [messageSend, setMessageSend] = useState<string>()
  const [nom, setNom] = useState<string>()
  const [sendMovieTime, setSendMovieTime] = useState<boolean>()
  const [timestampToSend, setTimestampToSend] = useState<number | null>(null)
  const timeStamp = useSelector(selectTimeStamp)
  const messages = useSelector(selectMessages);
  const dispatch = useDispatch();

  async function submitMessage() {
    let moment = null
    if (sendMovieTime) {
      moment = timestampToSend
    }
    const messageToSend : message = { 
      name: nom as string, 
      message: messageSend as string,
      when: (Date.now()).toString(),
      moment: moment
    }    
    props.webSocket.send(JSON.stringify(messageToSend))
    clearInputs()
  }

  const clearInputs = () => {
    // @ts-ignore
    document.getElementById("user").value = ""
    // @ts-ignore
    document.getElementById("message").value = ""
    // @ts-ignore
    document.getElementById("scales").checked = false
  }

  console.log(messages)

  return (
    <>

    <div>
      {messages.map( (msg : message, index: number) => {
        return (
          <div className='messageBox' key={"msg_"+index.toString()}>
            <p className='messageName'>{msg.name}</p>
            <p className='messageBody'>{msg.message}</p>
            {(msg.moment !== null && msg.moment !== undefined) && (
              <p 
                className='messageMoment'
                onClick={() => {
                  // @ts-ignore
                  dispatch(set(parseFloat(msg.moment)))
                  dispatch(needSeek(true))
                }}
              >
                {msg.moment}
              </p>
            )}
          </div>
        )
      })}
    </div>
	
    <div id='form-msg'>
      <p>Nom :</p>
      <input name="user" type="text" id="user" onChange={(e) => setNom(e.target.value)}/>
      <p>Message :</p>
      <input name="message" type="text" id="message" onChange={(e) => setMessageSend(e.target.value)}/>
      <p>Envoyer le timestamp du film ?</p>
      <input type="checkbox" id="scales" name="scales" 
        onChange={(e) => {
          setSendMovieTime(e.target.checked)
          setTimestampToSend(timeStamp)
        }}
      />
      <input name="submitmsg" type="submit"  id="submitmsg" value="Send" onClick={() => submitMessage()}/>
    </div>
  </>
  )
}

