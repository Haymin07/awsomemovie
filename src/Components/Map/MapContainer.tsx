import React from 'react'
import Map from './Map'
import { Marker, Popup } from 'react-leaflet'
import { LatLngExpression } from 'leaflet';
import { needSeek, selectTimeStamp, set } from '../../Store/timeStampSlice';
import { useDispatch, useSelector } from 'react-redux';

interface waypoint {
  label: string
  lat: string
  lng: string
  timestamp: string
}

interface MapContainerProps {
  waypoints : waypoint[],
}
export default function MapContainer(props: MapContainerProps) {
  const timeStamp = useSelector(selectTimeStamp);
  const dispatch = useDispatch();
  
  const getWaypointAndPos = () => {
    let i = 0
    let found = false

    // cas particulier pour les premières secondes de vidéo
    if (parseFloat(props.waypoints[0].timestamp) > timeStamp) {
      found = true
    }

    while (i < props.waypoints.length - 1 && !found) {
      if (
          parseFloat(props.waypoints[i].timestamp) <= timeStamp
          &&
          parseFloat(props.waypoints[i+1].timestamp) > timeStamp
        ){
        found = true
      } else {
        i ++
      }
    }

    const createdWaypoints = createTabMarkers(i)
    
    return ({
      markers: createdWaypoints,
      pos: [parseFloat(props.waypoints[i].lat), parseFloat(props.waypoints[i].lng)] as LatLngExpression
    })
  }

  const createTabMarkers = (activeIndex: number) => {
    const createdWaypoints: JSX.Element[] = []
    props.waypoints.forEach( (wp, index) => {
      let opacity = 0.5
      if (index === activeIndex) {
        opacity = 1
      }

      createdWaypoints.push(
        <Marker 
          position={[parseFloat(wp.lat), parseFloat(wp.lng)]}
          draggable={false}
          opacity={opacity}
          key={"marqueurs_n_"+index.toString()}
        >
          <Popup>
            {wp.label}
            <br/>
            {/* eslint-disable-next-line */}
            <a onClick={() => {
              dispatch(set(parseInt(wp.timestamp)))
              dispatch(needSeek(true))
            }}>
              Jump to : {convertTimeStamp(parseInt(wp.timestamp))}
            </a>
          </Popup>
        </Marker>
      )
    })
    return createdWaypoints
  }

  const convertTimeStamp = (timeStamp: number) => {
    let minutes = timeStamp/60
    let heures = 0
    if (minutes > 60) {
      heures = Math.floor(minutes/60)
      minutes = minutes - (60 * heures)
    }
    let minutes_str = minutes.toFixed(2)
    if (minutes < 10) {
      minutes_str = "0"+minutes_str
    }
    return heures.toString()+"."+minutes_str
  }

  const {markers,pos} = getWaypointAndPos()

  return (
    <Map 
      markers={markers}
      center={pos}
    />
  )
}
