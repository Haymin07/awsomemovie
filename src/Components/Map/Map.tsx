import React from 'react'
import { LatLngExpression } from 'leaflet';
import { MapContainer, TileLayer, useMap } from 'react-leaflet'

interface MapProps {
  markers: JSX.Element[],
  center: LatLngExpression
}

interface SetViewProps {
  coords: LatLngExpression
}

function SetView(props: SetViewProps) {
  const map = useMap();
  map.setView(props.coords, map.getZoom());

  return null;
}

export default function Map(props: MapProps) {
  return (
    <MapContainer 
      id='map'
      center={props.center}
      zoom={4}
    >
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {props.markers}
      <SetView coords={props.center} />
    </MapContainer>
  )
}
