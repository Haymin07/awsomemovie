export interface message{
  when:string,
  name:string,
  message:string,
  moment?:number | null
}
