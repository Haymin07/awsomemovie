import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  value: 0,
  haveToSeek: false
};

export const timeStampSlice = createSlice({
  name: 'timeStamp',
  initialState,
  reducers: {
    set: (state, action) => {
      state.value = action.payload;
    },
    needSeek: (state, action) => {
      state.haveToSeek = action.payload;
    }
  }
});

export const { set, needSeek } = timeStampSlice.actions;
export const selectTimeStamp = (state) => state.timeStamp.value;
export const selectSeek = (state) => state.timeStamp.haveToSeek;

export default timeStampSlice.reducer;
