import { configureStore } from '@reduxjs/toolkit';
import timeStampReducer from '../Store/timeStampSlice'
import messagesReducer from '../Store/messageSlice'

export const store = configureStore({
  reducer: {
    timeStamp: timeStampReducer,
    messages: messagesReducer
  },
});
