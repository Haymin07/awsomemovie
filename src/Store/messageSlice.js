import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  messages: []
};

export const messageSlice = createSlice({
  name: 'messages',
  initialState,
  reducers: {
    add: (state, action) => {
      action.payload.forEach(msg => {
        if(msg.name !== "Rick") {
          state.messages.push(msg);
        }
      });
    }
  }
});

export const { add } = messageSlice.actions;
export const selectMessages = (state) => state.messages.messages;

export default messageSlice.reducer;
