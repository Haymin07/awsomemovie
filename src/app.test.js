import React from 'react';
import { render } from '@testing-library/react';
import Main from './Components/Main';
import Chapters from './Components/Chapters';
import Keywords from './Components/Keywords';
import Video from './Components/Video';
import { store } from './Store/store';
import { Provider } from 'react-redux';


test('Render du composant Main', () => {
  render(
    <Provider store={store}>
      <Main />
    </Provider>
  );
})

test('Loading est affiché au render de Main', () => {
  const { getByText } = render(
  <Provider store={store}>
    <Main />
  </Provider>);
  const linkElement = getByText(/Loading/i);
  expect(linkElement).toBeInTheDocument();
});

test('Render du composant Chapters', () => {
  render(
    <Provider store={store}>
      <Chapters
        chapters= {[{"pos":"1120","title":"test"}]}
      />
    </Provider>
  )
})

test('Render du composant Keywords', () => {
  const keyword = {
    title: "Titre",
    url: "https://null.com"
  }
  const data = {
    data: [keyword,keyword],
    pos: "2300"
  }

  render(
    <Provider store={store}>
      <Keywords
        datas= {[data]}
      />
    </Provider>
  )
})

test('Render du composant Video', () => {
  render(
    <Provider store={store}>
      <Video
        movie={null}
      />
    </Provider>
  ) 
})

// test('Render du composant Map', () => {
//   const marker= <h1>test</h1>
//   const latlng = [5.0,3.0]

//   render(
//     <Provider store={store}>
//       <Map
//         markers={[marker]}
//         center={latlng}
//       />
//     </Provider>
//   ) 
//   // je comprends pas pourquoi ça fail... Leaflet ?
// })

// test('Render du composant MapContainer', () => {
//   const waypoint = {
//     laber: "label",
//     lat: "-1",
//     lng: "2",
//     timestamp: "3400"
//   }

//   render(
//     <Provider store={store}>
//       <MapContainer
//         waypoints={[waypoint,waypoint]}
//       />
//     </Provider>
//   ) 
//   // je comprends pas pourquoi ça fail... Leaflet ?
// })

test('au timestamp 3774, le keyword Chicago doit être écrit', () => {
  const keyword = {
    title: "Chicago",
    url: "https://null.com"
  }
  const data = {
    data: [keyword],
    pos: "3774"
  }

  const { getByText } = render(
    <Provider store={store}>
      <Keywords
        datas= {[data]}
      />
    </Provider>
  )

  const linkElement = getByText(/Chicago/i);
  expect(linkElement).toBeInTheDocument();
})
