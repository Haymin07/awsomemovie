import axios from "axios"

export async function fetchData() {
  try {
    const response = await axios.get("https://imr3-react.herokuapp.com/backend")
    return response.data
  } catch (error) {
    console.error(error);
    return -1
  }
}
