# AwsomeMovie



## Résumé

AwomeMovie permet de regarder un film mais pas que !
On peut également naviguer dans le film par un système de chapitrage.
Voir sur une carte les lieux du film ainsi que des mots clef selon votre avancement de lecture.
Vous pourrez également envoyer des messages et partager des moments grâce à un chat.

## Développé par 

Quentin LEMESLE
Marjorie LATGER

## Notre expérience

Quentin : 9 mois auparavant, j'ai travaillé 3 mois en entreprise avec React, c'était la première fois que j'utilisais cette technologie. Cette année j'ai utilisé React dans mon projet génie logiciel qui a duré 6 mois.

Marjorie : Cette année, j'ai utilisé React dans mon projet génie logiciel qui a duré 6 mois, c'était la première fois que j'utilisais cette technologie.

